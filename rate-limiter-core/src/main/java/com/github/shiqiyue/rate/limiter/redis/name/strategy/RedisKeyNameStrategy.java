package com.github.shiqiyue.rate.limiter.redis.name.strategy;

/***
 * redis key 的命名策略
 * 
 * @author wwy
 *
 */
public interface RedisKeyNameStrategy {
	
	/***
	 * 获得key的名称
	 * 
	 * @param props
	 * @return
	 */
	public String getKeyName(String... props);
	
	/***
	 * 获得锁的key的名称
	 * 
	 * @param props
	 * @return
	 */
	public String getLockKeyName(String... props);
}
