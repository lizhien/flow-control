package com.github.shiqiyue.rate.limiter.redis.name.strategy;

import org.springframework.util.StringUtils;

/***
 * redis key 的默认命名策略
 * 
 * @author wwy
 *
 */
public class DefaultRedisKeyNameStrategy implements RedisKeyNameStrategy {


	private static final String KEY_PREFIX = "flow:control";

	private static final String LOCK_KEY_PREFIX = "flow:control:key";

	
	@Override
	public String getKeyName(String... props) {
		if (props == null || props.length == 0) {
			throw new IllegalArgumentException("参数不能为空");
		}
		StringBuffer keySuffix = new StringBuffer();
		for (String prop : props) {
			if (!StringUtils.isEmpty(prop)) {
				keySuffix.append(":").append(prop);
			}
		}
		return KEY_PREFIX + keySuffix.toString();
	}
	
	@Override
	public String getLockKeyName(String... props) {
		if (props == null || props.length == 0) {
			throw new IllegalArgumentException("参数不能为空");
		}
		StringBuffer keySuffix = new StringBuffer();
		for (String prop : props) {
			keySuffix.append(":").append(prop);
		}
		
		return LOCK_KEY_PREFIX + keySuffix.toString();
	}
	
}
