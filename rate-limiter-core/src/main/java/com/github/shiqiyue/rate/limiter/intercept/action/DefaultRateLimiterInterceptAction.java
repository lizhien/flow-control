package com.github.shiqiyue.rate.limiter.intercept.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.Charset;

/***
 * 默认流量控制-拦截动作
 * 
 * @author wwy
 *
 */
public class DefaultRateLimiterInterceptAction implements RateLimiterInterceptAction {
	
	private String jsonResponse = "{\"code\":400,\"mes\":\"\u8BBF\u95EE\u592A\u8FC7\u9891\u7E41\uFF0C\u8BF7\u8FC7\u6BB5\u65F6\u95F4\u518D\u8BD5\"}";
	
	@Override
	public void action(HttpServletRequest request, HttpServletResponse response) {
		String responseMes = jsonResponse;
		response.setContentType("application/json;charset=UTF-8");
		try {
			response.getOutputStream().write(responseMes.getBytes(Charset.forName("UTF-8")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public String getJsonResponse() {
		return jsonResponse;
	}
	
	public void setJsonResponse(String jsonResponse) {
		this.jsonResponse = jsonResponse;
	}
	
}
